package com.onavo.count.model.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Class that wraps the most common database operations. This example assumes you want a single table and data entity
 * with two properties: a title and a priority as an integer. Modify in all relevant locations if you need other/more
 * properties for your data and/or additional tables.
 */
public class DatabaseHelper {
    private SQLiteOpenHelper _openHelper;

    /**
     * Construct a new database helper object
     * @param context The current context for the application or activity
     */
    public DatabaseHelper(Context context) {
        _openHelper = new SimpleSQLiteOpenHelper(context);
    }

    /**
     * This is an internal class that handles the creation of all database tables
     */
    class SimpleSQLiteOpenHelper extends SQLiteOpenHelper {
        SimpleSQLiteOpenHelper(Context context) {
            super(context, "onavomain.db", null, 14);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("create table delays (_id integer primary key autoincrement, network text, url text, delayinsec text, dates text)");
            db.execSQL("create table datausage (_id integer primary key autoincrement, totalMbs text, dates datetime)");
            /*db.execSQL("create table dataquotas (_id integer primary key autoincrement, limit text, mbsDataUsed text)");*/
            Log.e("DB ON CREATE", "Creating databases");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        	db.execSQL("DROP TABLE IF EXISTS delays");
        	db.execSQL("DROP TABLE IF EXISTS datausage");
        	/*db.execSQL("DROP TABLE IF EXISTS dataquotas");*/
        	Log.e("DB ON UPGRADE", "Clearing databases");
            onCreate(db);
        }
    }

    /**
     * Return a cursor object with all rows in the table.
     * @return A cursor suitable for use in a SimpleCursorAdapter
     */
    public Cursor getAll(String tableName) {
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        if (db == null) {
            return null;
        }
        Log.e("DB GET ALL", "From "+tableName);
        return db.rawQuery("select * from "+tableName+" order by _id", null);
    }

    /**
     * Return values for a single row with the specified id
     * @param id The unique id for the row o fetch
     * @return All column values are stored as properties in the ContentValues object
     */
    public ContentValues get(String tableName, long id) {
        SQLiteDatabase db = _openHelper.getReadableDatabase();
        if (db == null) {
            return null;
        }
        ContentValues row = new ContentValues();
        Cursor cur;
        if(tableName == "delays"){
        	cur = db.rawQuery("select network, url, dates from delays where _id = ?", new String[] { String.valueOf(id) });
        	if (cur.moveToNext()) {
                row.put("network", cur.getString(0));
                row.put("url", cur.getInt(1));
                row.put("dates", cur.getInt(2));
            }
        }else{
        	cur = db.rawQuery("select totalMbs, dates from datausage where _id = ?", new String[] { String.valueOf(id) });
        	if (cur.moveToNext()) {
                row.put("megabytes", cur.getString(0));
                row.put("dates", cur.getInt(1));
            }
        }
        
        cur.close();
        db.close();
        Log.e("DB GET BY ID", "Getting from "+tableName+" row id is "+id);
        return row;
    }

    /**
     * Add a new row to the database table
     * @param title The title value for the new row
     * @param priority The priority value for the new row
     * @return The unique id of the newly added row
     */
    public long add(String tableName, ContentValues values) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return 0;
        }

        long id = db.insert(tableName, null, values);
        Log.e("DB INSERT", "Inserting to "+tableName+" transaction id is "+id);
        db.close();
        return id;
    }

    /**
     * Delete the specified row from the database table. For simplicity reasons, nothing happens if
     * this operation fails.
     * @param id The unique id for the row to delete
     */
    public void delete(String tableName, long id) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return;
        }
        db.delete(tableName, "_id = ?", new String[] { String.valueOf(id) });
        db.close();
        Log.e("DB DELETE", "Deleting from "+tableName+" row id is "+id);
    }

    /**
     * Updates a row in the database table with new column values, without changing the unique id of the row.
     * For simplicity reasons, nothing happens if this operation fails.
     * @param id The unique id of the row to update
     * @param title The new title value
     * @param priority The new priority value
     */
    public void update(long id, String tableName, ContentValues values) {
        SQLiteDatabase db = _openHelper.getWritableDatabase();
        if (db == null) {
            return;
        }
        db.update(tableName, values, "_id = ?", new String[] { String.valueOf(id) } );
        db.close();
        Log.e("DB UPDATE", "Updating from "+tableName+" row id is "+id);
    }
}
