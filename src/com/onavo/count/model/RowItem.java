package com.onavo.count.model;

public class RowItem {

    private String title;
    private String subTitle;
    private long time;

    public RowItem(String title, String sub, long time) {
        this.title = title;
        this.subTitle = sub;
        this.time = time;

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subTitle;
    }

    public void setSubtitle(String subTitle) {
        this.subTitle = subTitle;
    }
    
    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

}