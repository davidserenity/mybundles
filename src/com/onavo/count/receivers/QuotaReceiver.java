package com.onavo.count.receivers;

import com.onavo.count.services.QuotaService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class QuotaReceiver extends BroadcastReceiver {
  public static final int REQUEST_CODE = 123456;

  // Triggered by the Alarm periodically (starts the service to run task)
  @Override
  public void onReceive(Context context, Intent intent) {
    Intent i = new Intent(context, QuotaService.class);
    context.startService(i);
    Log.d("Quota Receiver", "Receiving Quota Alarm");
  }
}
