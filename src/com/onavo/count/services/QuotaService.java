package com.onavo.count.services;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.onavo.count.MainActivity;
import com.onavo.count.model.helper.DatabaseHelper;

import android.R;
import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.TrafficStats;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

@SuppressLint("NewApi")
public class QuotaService extends IntentService {
    // Must create a default constructor
    public QuotaService() {
        // Used to name the worker thread, important only for debugging.
        super("QuotaService");
    }
    
    DatabaseHelper db;

    @Override
    public void onCreate() {
        super.onCreate(); // if you override onCreate(), make sure to call super().
        // If a Context object is needed, call getApplicationContext() here.
        db = new DatabaseHelper(getApplicationContext());
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy(); // if you override onCreate(), make sure to call super().
        // If a Context object is needed, call getApplicationContext() here.
        Log.d("Service Destroy", "QuotaService destroyed");
    }

    ArrayList<String> alldbTimes;
    String todayTime = getTime();
    String earliestDataRead;
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences sharedpreferences;
    float dataLimit;
    int dataUnit;
    
    
	@Override
    protected void onHandleIntent(Intent intent) {
    	Log.d("Quota Service started", "Running QuotaService");
    	sharedpreferences = getApplicationContext().getSharedPreferences(MyPREFERENCES, getApplicationContext().MODE_PRIVATE);
    	dataLimit = Float.parseFloat(sharedpreferences.getString("quota", null));
    	dataUnit = sharedpreferences.getInt("data", 0);
    	
        // This describes what will happen when service is triggered
    	String [] splitToday = todayTime.split("\\s+");
    	long rxBytes = TrafficStats.getMobileRxBytes();
    	long txBytes = TrafficStats.getMobileTxBytes();
    	long totalBytes = rxBytes + txBytes; 
    	
    	Cursor c = db.getAll("datausage");
    	
		while(c.moveToNext()) {
			String[] splitDBDate = c.getString(c.getColumnIndexOrThrow("dates")).split("\\s+");
			String[] splitDBTime = splitDBDate[1].split(":");

			if(splitToday[0].contains(splitDBDate[0])){
				earliestDataRead = c.getString(c.getColumnIndexOrThrow("totalMbs"));
				break;
			}
		}
		
		Long bytesUsed = totalBytes - Long.parseLong(earliestDataRead);
		float megaBytesUsed = bytesToMBs(bytesUsed);
		
		if(sharedpreferences.getBoolean("disable", false)){
			Log.d("Disable data", "Its active");
			//Data unit is Megabytes
			if(dataUnit == 0){
				disableData(megaBytesUsed);
			}//Data unit is Gigabytes
			else{
				float gigaBytesUsed = (float) (megaBytesUsed * 0.001);
				disableData(gigaBytesUsed);
			}	
		}else{
			Log.d("Disable data", "Its not active");
		}
		
		Log.d("Total MBs Used Up to Now", bytesToMBs(totalBytes)+"");
		Log.d("Earliest MBs Used", bytesToMBs(Long.parseLong(earliestDataRead))+"");
		Log.d("Total MBs Used", bytesToMBs(bytesUsed)+"");
	}
    
    public void disableData(float dataUsed){
    	if(dataUsed >= dataLimit ){
    		Log.d("Disable data", "Greater than limit");
    		displaySimpleNote();
			//Toast.makeText(getApplicationContext(),"Exceeded Data Limit, Mobile Data Disabled",Toast.LENGTH_LONG).show();
			try {
				setMobileDataEnabled(getApplicationContext(), false);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ReflectiveOperationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			Log.d("Disable data", "Less than limit");
		}
    }
    
    @SuppressWarnings("deprecation")
	public void displaySimpleNote(){
    	/*Notification note = new Notification(R.drawable.ic_dialog_alert, "Exceeded Data Limit, Mobile Data Disabled", System.currentTimeMillis());
    	PendingIntent intent = PendingIntent.getActivity(getBaseContext(), 0, new Intent(getBaseContext(), MainActivity.class), 0);
    	note.setLatestEventInfo(getBaseContext(), "MyBundles", "Mobile Data Disabled", intent);*/
    	
    	NotificationCompat.Builder mBuilder =
    	        new NotificationCompat.Builder(this)
    	        .setSmallIcon(R.drawable.ic_dialog_alert)
    	        .setContentTitle("MyBundles")
    	        .setContentText("Mobile Data Disabled!");
    	// Creates an explicit intent for an Activity in your app
    	Intent resultIntent = new Intent(this, MainActivity.class);

    	// The stack builder object will contain an artificial back stack for the
    	// started Activity.
    	// This ensures that navigating backward from the Activity leads out of
    	// your application to the Home screen.
    	TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
    	// Adds the back stack for the Intent (but not the Intent itself)
    	stackBuilder.addParentStack(MainActivity.class);
    	// Adds the Intent that starts the Activity to the top of the stack
    	stackBuilder.addNextIntent(resultIntent);
    	PendingIntent resultPendingIntent =
    	        stackBuilder.getPendingIntent(
    	            0,
    	            PendingIntent.FLAG_UPDATE_CURRENT
    	        );
    	mBuilder.setContentIntent(resultPendingIntent);
    	NotificationManager mNotificationManager =
    	    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    	int mId = 1234567;
		// mId allows you to update the notification later on.
    	mNotificationManager.notify(mId , mBuilder.build());
    }

    public String getTime(){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		String test = sdf.format(cal.getTime());
		Log.e("TEST TIME", test);
		return test;
	}
    
    public float bytesToMBs(long bytes) {
		//since 1byte = 0.001 Kilobytes;
		//1 kilobyte = 0.001 megabytes
		float megabytes = (float) (bytes * 0.001 * 0.001);
		return  megabytes;
	}
    
	private void setMobileDataEnabled(Context context, boolean enabled) throws ClassNotFoundException, ReflectiveOperationException {
	    final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    final Class conmanClass = Class.forName(conman.getClass().getName());
	    final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
	    iConnectivityManagerField.setAccessible(true);
	    final Object iConnectivityManager = iConnectivityManagerField.get(conman);
	    final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
	    final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
	    setMobileDataEnabledMethod.setAccessible(true);
	    setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
	}
}