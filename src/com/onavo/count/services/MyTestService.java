package com.onavo.count.services;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.onavo.count.model.helper.DatabaseHelper;

import android.app.Activity;
import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.net.TrafficStats;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class MyTestService extends IntentService {
    // Must create a default constructor
    public MyTestService() {
        // Used to name the worker thread, important only for debugging.
        super("test-service");
    }
    
    DatabaseHelper db;

    @Override
    public void onCreate() {
        super.onCreate(); // if you override onCreate(), make sure to call super().
        // If a Context object is needed, call getApplicationContext() here.
        db = new DatabaseHelper(getApplicationContext());
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy(); // if you override onCreate(), make sure to call super().
        // If a Context object is needed, call getApplicationContext() here.
        Log.d("Service Destroy", "MyTestService Destroyed");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // This describes what will happen when service is triggered
    	
    	long rxBytes = TrafficStats.getMobileRxBytes();
    	long txBytes = TrafficStats.getMobileTxBytes();
    	long totalBytes = rxBytes + txBytes; 
    	
    	//Save to database
		ContentValues rw = new ContentValues();
		rw.put("totalMbs", totalBytes);
		rw.put("dates", getTime());
		db.add("datausage", rw);
		Log.d("Service Handle", "MyTestService has finished work");
    }
    
    public String getTime(){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		String test = sdf.format(cal.getTime());
		Log.e("TEST TIME", test);
		return test;
	}
}