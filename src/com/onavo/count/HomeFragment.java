package com.onavo.count;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.onavo.count.model.helper.DatabaseHelper;

import android.app.Fragment;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class HomeFragment extends Fragment {
	BarData data;
	BarChart chart;
	DatabaseHelper db;
	ArrayList<String> currentdbDates;
	ArrayList<String> currentdbData;
	ArrayList<String> alldbDates;

	String todayTime;

	TextView dateScroller;

	public HomeFragment(){}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_home, container, false);
		return rootView;
	}

	ArrayList<BarEntry> entries;
	String[] splitToday;
	int dateTracker = 1;
	Button next;
	Button prev;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		todayTime = getTime();
		next = (Button) getView().findViewById(R.id.nextDate);
		next.setVisibility(View.GONE);
		prev = (Button) getView().findViewById(R.id.prevDate);
		dateScroller = (TextView) getView().findViewById(R.id.dataUsageDate);
		dateScroller.setText("Today");
		currentdbDates = new ArrayList<String>();
		currentdbData = new ArrayList<String>();
		alldbDates = new ArrayList<String>();

		db = new DatabaseHelper(getActivity());

		splitToday = todayTime.split("\\s+");

		prev.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {    
				next.setVisibility(View.VISIBLE);
				drawGraph(1);
			}
		});
		
		next.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {    
				Log.d("Next", "Its next");
				prev.setVisibility(View.VISIBLE);
				drawGraph(2);
			}
		});

		//Draw todays graph
		drawGraph(0);
	}

	public void loadDataFromDB(int direction){
		Cursor c = db.getAll("datausage");
		currentdbDates = new ArrayList<String>();
		currentdbData = new ArrayList<String>();

		switch(direction){
		case 0:
			//Load data for today
			loadDataForToday(c);
			
			//Check if there is any data saved before today
			//to decide whether we display previous button
			if(alldbDates.size() <= 1) {
				prev.setVisibility(View.GONE);				
			}else {
				prev.setVisibility(View.VISIBLE);
				Log.d("Lesser than 0", alldbDates.size()+"");
			}
			
			break;
		case 1:
			//Load previous data
			loadPreviousData(c);
			break;
		case 2:
			//Load next data
			loadNextData(c);
			break;
		default:
			break;
		}
	}

	public void loadDataForToday(Cursor c){
		while(c.moveToNext()) {
			String[] splitDBDate = c.getString(c.getColumnIndexOrThrow("dates")).split("\\s+");
			String[] splitDBTime = splitDBDate[1].split(":");

			if(!alldbDates.contains(splitDBDate[0]))
				alldbDates.add(splitDBDate[0]);

			if(splitToday[0].contains(splitDBDate[0])){
				currentdbDates.add(splitDBTime[0]);
				currentdbData.add(c.getString(c.getColumnIndexOrThrow("totalMbs")));	
			}
		}
	}

	public void loadPreviousData(Cursor c) {
		//if(alldbDates.size() > 0) {
			try {
				String dateStr = alldbDates.get(alldbDates.size() - 1 - dateTracker);
				dateTracker++;

				loadCurrentDBYvalues(c, dateStr);
				
				if(dateTracker > (alldbDates.size() - 1)){
					next.setVisibility(View.VISIBLE);
					prev.setVisibility(View.GONE);
					//Subtract by two to allow next function start from second last item
					dateTracker = dateTracker - 2;
				}

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		//}
	}

	public void loadNextData(Cursor c) {
		try {
			String dateStr = alldbDates.get(alldbDates.size() - 1 - dateTracker);
			dateTracker--;

			loadCurrentDBYvalues(c, dateStr);
			
			if(dateTracker < 0){
				prev.setVisibility(View.VISIBLE);
				next.setVisibility(View.GONE);
				dateTracker = 1;
				dateScroller.setText("Today");
			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	public String getCurrentDate(String dateStr) throws ParseException{
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
		SimpleDateFormat display = new SimpleDateFormat("EEEE, dd-MMM-yyyy");
		cal.setTime(sdf.parse(dateStr));
		String prevDate = sdf.format(cal.getTime());
		dateScroller.setText(display.format(cal.getTime()));
		return prevDate;
	}
	
	public void loadCurrentDBYvalues(Cursor c, String dateStr) throws IllegalArgumentException, ParseException{
		while(c.moveToNext()) {
			String[] splitDBDate = c.getString(c.getColumnIndexOrThrow("dates")).split("\\s+");
			String[] splitDBTime = splitDBDate[1].split(":");

			if(getCurrentDate(dateStr).contains(splitDBDate[0])){
				currentdbDates.add(splitDBTime[0]);
				currentdbData.add(c.getString(c.getColumnIndexOrThrow("totalMbs")));	
			}
		}
	}
	ArrayList<String> yValues = new ArrayList<String>();
	public BarDataSet getYAxisValues(int direction){
		loadDataFromDB(direction);
		entries = new ArrayList<BarEntry>();
		int count = 0;
		for(int x=0; x<24; x++){
			int firstIndex;
			int lastIndex;
			Long lastMbs;
			Long firstMbs;
			if(x<10){
				firstIndex = currentdbDates.indexOf("0"+x);
				lastIndex = currentdbDates.lastIndexOf("0"+(x+1));
			}else {
				firstIndex = currentdbDates.indexOf(x+"");
				lastIndex = currentdbDates.lastIndexOf((x+1)+"");
			}

			if(firstIndex == -1){
				firstMbs = (long) 0;
				lastMbs = (long) 0;
			}else{
				firstMbs = Long.parseLong(currentdbData.get(firstIndex));
				if(lastIndex == -1){
					lastMbs = (long) 0;
					firstMbs = (long) 0;
				}else{
					lastMbs = Long.parseLong(currentdbData.get(lastIndex));
				}
			}
			
			Long totalMbs = lastMbs - firstMbs;
			float entry = bytesToMBs(Math.abs(totalMbs));
			yValues.add(entry+"");
			entries.add(new BarEntry(entry, count));

			//Log.d("counter", count+" direction is "+direction);
			count = count + 1;
		}
		BarDataSet dataset = new BarDataSet(entries, "Data in MBs");
		return dataset;
	}

	private ArrayList<String> getXAxisValues() {
		ArrayList<String> xAxis = new ArrayList<String>();
		xAxis.add("00:00");
		xAxis.add("01:00");
		xAxis.add("02:00");
		xAxis.add("03:00");
		xAxis.add("04:00");
		xAxis.add("05:00");
		xAxis.add("06:00");
		xAxis.add("07:00");
		xAxis.add("08:00");
		xAxis.add("09:00");
		xAxis.add("10:00");
		xAxis.add("11:00");
		xAxis.add("12:00");
		xAxis.add("13:00");
		xAxis.add("14:00");
		xAxis.add("15:00");
		xAxis.add("16:00");
		xAxis.add("17:00");
		xAxis.add("18:00");
		xAxis.add("19:00");
		xAxis.add("20:00");
		xAxis.add("21:00");
		xAxis.add("22:00");
		xAxis.add("23:00");
		return xAxis;
	}

	public void drawGraph(int direction){
		chart = (BarChart)getView().findViewById(R.id.content_bar_chart);
		data = new BarData(getXAxisValues(), getYAxisValues(direction));
		if(alldbDates.size() > 0){
			for(int x= 0; x<yValues.size() - 1; x++){
				if(!yValues.get(x).contains("0.0")){
					//Log.d("yValues data if", yValues.get(x));
					chart.setData(data);
					chart.setDescription("Data Usage per day");
					chart.animateXY(2000, 2000);
					if(direction == 1 || direction == 2){
						chart.notifyDataSetChanged();
					}

					chart.invalidate();
					break;
				}
			}
		}
	}

	public float bytesToMBs(long bytes) {
		//since 1byte = 0.001 Kilobytes;
		//1 kilobyte = 0.001 megabytes
		float megabytes = (float) (bytes * 0.001 * 0.001);
		return  megabytes;
	}

	public String getTime(){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		String test = sdf.format(cal.getTime());
		Log.e("TEST TIME", test);
		return test;
	} 
}
