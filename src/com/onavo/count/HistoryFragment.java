package com.onavo.count;

import java.util.ArrayList;

import com.onavo.count.adapter.CustomAdapter;
import com.onavo.count.adapter.DataUsageAdapter;
import com.onavo.count.model.RowItem;
import com.onavo.count.model.helper.DatabaseHelper;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class HistoryFragment extends Fragment {
	DatabaseHelper db;
	
	public HistoryFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.history, container, false);
        
        return rootView;
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		db = new DatabaseHelper(getActivity());
		// Find ListView to populate
		ListView lvItems = (ListView) getView().findViewById(R.id.historylist);
		// Setup cursor adapter using cursor from last step
		DataUsageAdapter todoAdapter = new DataUsageAdapter(getActivity(), db.getAll("delays"));
		// Attach cursor adapter to the ListView 
		lvItems.setAdapter(todoAdapter);

	}
}
