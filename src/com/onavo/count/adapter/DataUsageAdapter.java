package com.onavo.count.adapter;

import com.onavo.count.R;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class DataUsageAdapter extends CursorAdapter {
	  public DataUsageAdapter(Context context, Cursor cursor) {
	      super(context, cursor, 0);
	  }

	  // The newView method is used to inflate a new view and return it, 
	  // you don't bind any data to the view at this point. 
	  @Override
	  public View newView(Context context, Cursor cursor, ViewGroup parent) {
	      return LayoutInflater.from(context).inflate(R.layout.history_list_item, parent, false);
	  }

	  // The bindView method is used to bind all data to a given view
	  // such as setting the text on a TextView. 
	  @Override
	  public void bindView(View view, Context context, Cursor cursor) {
	      // Find fields to populate in inflated template
	      TextView title = (TextView) view.findViewById(R.id.historytitle);
	      TextView subtitle = (TextView) view.findViewById(R.id.historysubTitle);
	      //TextView timedate = (TextView) view.findViewById(R.id.historyTime);
	      //TextView timing = (TextView) view.findViewById(R.id.historytiming);
	      // Extract properties from cursor
	      
	      //String ttl = cursor.getString(cursor.getColumnIndexOrThrow("network"));
	      String subttl = cursor.getString(cursor.getColumnIndexOrThrow("url"));
	      //String time = cursor.getString(cursor.getColumnIndexOrThrow("delayinsec"));
	      String timesecs = cursor.getString(cursor.getColumnIndexOrThrow("dates"));
	      
	      // Populate fields with extracted properties
	      title.setText(timesecs);
	      subtitle.setText(subttl);
	      //timedate.setText(timesecs);
	      //timing.setText(time);
	  }
	}
