package com.onavo.count.adapter;

import java.util.List;

import com.onavo.count.R;
import com.onavo.count.model.RowItem;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomAdapter extends BaseAdapter {

    Context context;
    List<RowItem> rowItem;

    public CustomAdapter(Context context, List<RowItem> rowItem) {
        this.context = context;
        this.rowItem = rowItem;

    }
    
    public void add(List<RowItem> rowItem){
    	this.rowItem = rowItem;
    }

    @Override
    public int getCount() {

        return rowItem.size();
    }

    @Override
    public Object getItem(int position) {

        return rowItem.get(position);
    }

    @Override
    public long getItemId(int position) {

        return rowItem.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_item, null);
        }

        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        TextView subTitle = (TextView) convertView.findViewById(R.id.subTitle);
        TextView timing = (TextView) convertView.findViewById(R.id.timing);

        RowItem row_pos = rowItem.get(position);
        // setting the image resource and title
        
        txtTitle.setText(row_pos.getTitle());
        subTitle.setText(row_pos.getSubtitle());
        timing.setText((String.valueOf(row_pos.getTime())+" s"));

        return convertView;

    }

}