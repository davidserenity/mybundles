package com.onavo.count;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

import com.onavo.count.adapter.CustomAdapter;
import com.onavo.count.model.RowItem;
import com.onavo.count.model.helper.DatabaseHelper;
import com.onavo.count.receivers.MyAlarmReceiver;
import com.onavo.count.receivers.QuotaReceiver;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Fragment;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SettingsFragment extends Fragment {
	EditText quotaField;
	String[] arraySpinner;
	SharedPreferences sharedpreferences;
	public static final String MyPREFERENCES = "MyPrefs" ;
	public SettingsFragment(){}
	
	String quotaValue;
	int dataUnitValue;
	int timePeriodValue;
	Boolean disableDataValue;
	Boolean realtimeDataValue;
	Boolean hourlyDataValue;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.settings, container, false);
         
        return rootView;
    }
	
	AlarmManager alarm;
	PendingIntent pIntent;
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		alarm = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
		// Construct an intent that will execute the AlarmReceiver
		Intent intent = new Intent(getActivity(), QuotaReceiver.class);
		// Create a PendingIntent to be triggered when the alarm goes off
		pIntent = PendingIntent.getBroadcast(getActivity(), QuotaReceiver.REQUEST_CODE,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);

		sharedpreferences = getActivity().getSharedPreferences(MyPREFERENCES, getActivity().MODE_PRIVATE);
		
		quotaField = (EditText) getView().findViewById(R.id.quota);
		quotaField.setText(sharedpreferences.getString("quota", null));
		
		final Spinner dataUnit = (Spinner) getView().findViewById(R.id.dataunits);
		dataUnit.setSelection(sharedpreferences.getInt("data", 0));
		/*Log.d("DataUnit set is", sharedpreferences.getInt("data", 0)+"");*/
		
		final Spinner timePeriod = (Spinner) getView().findViewById(R.id.timeperiods);
		timePeriod.setSelection(sharedpreferences.getInt("period", 0));
		
		final CheckBox disableData = (CheckBox) getView().findViewById(R.id.disabledata);
		disableData.setChecked(sharedpreferences.getBoolean("disable", false));
		/*Log.d("Disable data", sharedpreferences.getBoolean("disable", false)+"");*/
		
		final CheckBox realtimeData = (CheckBox) getView().findViewById(R.id.realtimedata);
		realtimeData.setChecked(sharedpreferences.getBoolean("realtime", false));
		
		/*final CheckBox hourlyData = (CheckBox) getView().findViewById(R.id.hourlydata);
		hourlyData.setChecked(sharedpreferences.getBoolean("hourly", false));*/
		
		final Button saveSettings = (Button) getView().findViewById(R.id.savesettings);
		
		saveSettings.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {        
				
				quotaValue = quotaField.getText().toString().trim();
				dataUnitValue = dataUnit.getSelectedItemPosition();
				timePeriodValue = timePeriod.getSelectedItemPosition();
				disableDataValue = disableData.isChecked();
				realtimeDataValue = realtimeData.isChecked();
				//hourlyDataValue = hourlyData.isChecked();
				
				SharedPreferences.Editor editor = sharedpreferences.edit();
			    editor.putString("quota", quotaValue);
			    editor.putInt("data", dataUnitValue);
			    editor.putInt("period", timePeriodValue);
			    editor.putBoolean("disable", disableDataValue);
			    editor.putBoolean("realtime", realtimeDataValue);
			    //editor.putBoolean("hourly", hourlyDataValue);
			    editor.commit();
			    
			    resetAlarm();
				Toast.makeText(getActivity(),"Saved",Toast.LENGTH_LONG).show();
			}
		});
	}
	
	public void resetAlarm(){
		//cancelAlarm();
		//Check if realtime data monitoring is active
		if(sharedpreferences.getBoolean("realtime", false)){
			Log.d("Realtime data monitor", "Quota Realtime Its active");
			scheduleAlarm(0);
		}else{
			Log.d("Realtime data monitor", "Quota Realtime Its not active");
			scheduleAlarm(1);
		}
	}

	// Setup a recurring alarm
	public void scheduleAlarm(int indx) {
		long [] interval = new long[2];
		interval[0] = AlarmManager.ELAPSED_REALTIME;
		interval[1] = AlarmManager.INTERVAL_FIFTEEN_MINUTES;
		
		// Setup periodic alarm every 5 seconds
		long firstMillis = System.currentTimeMillis(); // alarm is set right away
		
		// First parameter is the type: ELAPSED_REALTIME, ELAPSED_REALTIME_WAKEUP, RTC_WAKEUP
		// Interval can be INTERVAL_FIFTEEN_MINUTES, INTERVAL_HALF_HOUR, INTERVAL_HOUR, INTERVAL_DAY
		alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis, interval[indx], pIntent);
	}
	
	public void cancelAlarm(){
		alarm.cancel(pIntent);
	}
}
