package com.onavo.count;

import android.app.AlertDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.net.TrafficStats;

import java.util.Random;

import org.codeandmagic.android.gauge.GaugeView;

public class DataRatesFragment extends Fragment {
	//Gauge variables
	private GaugeView mGaugeView1;
	private final Random RAND = new Random();
	
	//Traffic stats variables
	private Handler mHandler = new Handler();
	private long mStartRX = 0;
	private long mStartTX = 0;
	long startTime;
	long recordedTime;
	TextView RX;
	TextView TX;
	
	public DataRatesFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_photos, container, false);
        
        mGaugeView1 = (GaugeView) rootView.findViewById(R.id.gauge_view1);
        
        RX = (TextView)rootView.findViewById(R.id.RX);
    	TX = (TextView)rootView.findViewById(R.id.TX);
        
        startTime = System.currentTimeMillis();
        mStartRX = TrafficStats.getMobileRxBytes();
        mStartTX = TrafficStats.getMobileTxBytes();
        
        if (mStartRX == TrafficStats.UNSUPPORTED || mStartTX == TrafficStats.UNSUPPORTED) {
        	AlertDialog.Builder alert = new AlertDialog.Builder(this.getActivity());
			alert.setTitle("Uh Oh!");
			alert.setMessage("Your device does not support traffic stat monitoring.");
			alert.show();
        } else {
        	mHandler.postDelayed(mRunnable, 1000);
        }
        
        return rootView;
    }
	
	private final Runnable mRunnable = new Runnable() {
        public void run() {
        	//Code to return mega bits
        	/*//downloaded bytes
        	long rxBytes = TrafficStats.getTotalRxBytes()- mStartRX;
        	long rxMBits = bytesToMBs(rxBytes);
        	recordedTime = (System.currentTimeMillis() - startTime) / 1000;
        	long rxMBitsPersec = rxMBits / recordedTime;
        	RX.setText(Long.toString(rxMBitsPersec)+" Mbps");
        	mGaugeView1.setTargetValue(rxMBitsPersec);
        	
        	//Uploaded bytes
        	long txBytes = TrafficStats.getTotalTxBytes()- mStartTX;
        	long txMBits = bytesToMBs(txBytes);
        	long txMBitsPersec = txMBits / recordedTime;
        	TX.setText(Long.toString(txMBitsPersec)+" Mbps");*/
        	
        	//downloaded bytes
        	long rxBytes = TrafficStats.getMobileRxBytes()- mStartRX;
        	long rxKBits = bytesToKBs(rxBytes);
        	recordedTime = (System.currentTimeMillis() - startTime) / 1000;
        	long rxKBitsPersec = rxKBits / recordedTime;
        	RX.setText(Long.toString(rxKBitsPersec)+" Kbps");
        	mGaugeView1.setTargetValue(rxKBitsPersec);
        	
        	//Uploaded bytes
        	long txBytes = TrafficStats.getMobileTxBytes()- mStartTX;
        	long txKBits = bytesToKBs(txBytes);
        	long txKBitsPersec = txKBits / recordedTime;
        	TX.setText(Long.toString(txKBitsPersec)+" Kbps");
        	
        	mHandler.postDelayed(mRunnable, 1000);
        }
     };

     public static long bytesToKBs(long bytes) {
    	 //since 1byte = 8 bits;
    	 long bits = bytes * 8;
    	 //1 bit = 0.001 kilobits
    	 long kilobits = (long) (bits * 0.001);
    	 return  kilobits;
     }

     public static long bytesToMBs(long bytes) {
    	 //since 1byte = 8 bits;
    	 long bits = bytes * 8;
    	 //1 bit = 0.001 kilobits
    	 long kilobits = (long) (bits * 0.001);
    	 
    	 //1 kilobit = 0.001 megabits
    	 long megabits = (long) (kilobits * 0.001);
    	 return  megabits;
     }
}
