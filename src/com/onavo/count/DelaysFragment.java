package com.onavo.count;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.onavo.count.adapter.*;
import com.onavo.count.model.*;
import com.onavo.count.model.helper.DatabaseHelper;

import android.app.Fragment;
import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DelaysFragment extends Fragment implements OnItemClickListener {
	
	public DelaysFragment(){}
	DatabaseHelper db;
	long startTime;
	long recordedTime;

	String[] menutitles;
	TypedArray menuIcons;
	ArrayList timing = new ArrayList();

	CustomAdapter adapter;
	private List<RowItem> rowItems;

	ListView lv;

	EditText searchField;
	String urlText;

	String type;

	ProgressDialog progress;
	TextView errorView;
	
	private WebView mWebView = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.delays, null, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {

		super.onActivityCreated(savedInstanceState);
		db = new DatabaseHelper(getActivity());
		
		rowItems = new ArrayList<RowItem>();

		lv = (ListView) getView().findViewById(R.id.list);

		adapter = new CustomAdapter(getActivity(), rowItems);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);

		progress = new ProgressDialog(getActivity());
		progress.setTitle("Loading");
		progress.setMessage("Please wait...");
		progress.setCanceledOnTouchOutside(false);
		
		mWebView = (WebView) getView().findViewById(R.id.webView);
		mWebView.setWebViewClient(client);

		errorView = (TextView) getView().findViewById(R.id.error);
		searchField = (EditText) getView().findViewById(R.id.searchText1);
		final Button upButton = (Button) getView().findViewById(R.id.startSearch);
		upButton.setEnabled(false); 
		upButton.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {            	
				urlText = searchField.getText().toString().trim();
				if(urlText.length() > 0){
					type = Connectivity.isConnectedFast(getActivity());

					if(!type.contains("No NetWork Access")){
						if(urlText.toLowerCase().indexOf("http://") == -1) {
							urlText = "http://"+urlText;
						}
						//new RetrievePage().execute(urlText);
						mWebView.getSettings().setJavaScriptEnabled(true);
				        mWebView.loadUrl(urlText);
	
					}else{
						Toast.makeText(getActivity(), "No Network Access", Toast.LENGTH_LONG).show();
					}	
				}else{
					Toast.makeText(getActivity(), "Enter URL to Test Delay", Toast.LENGTH_LONG).show();
				}

			}
		});

		searchField.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {}

			@Override    
			public void beforeTextChanged(CharSequence s, int start,
					int count, int after) {
			}

			@Override    
			public void onTextChanged(CharSequence s, int start,
					int before, int count) {
				if(s.length() != 0)
					urlText = searchField.getText().toString().trim();
					if(Patterns.WEB_URL.matcher(urlText).matches()){
						errorView.setText("");
						upButton.setEnabled(true);
					}else{
						errorView.setText("Please use a valid URL without spaces.");
						upButton.setEnabled(false);
					}

			}
		});

	}
	
	WebViewClient client = new WebViewClient() {
		Boolean alreadyRun = false;
		@Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
			progress.show();
			startTime = System.currentTimeMillis();

        }
		
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
        	//view.loadUrl(url);
        	return false;
        }

        @Override
        public void onLoadResource(WebView view, String url) {
        	super.onLoadResource(view, url);
        	
        	if(mWebView.getProgress() == 100 && !alreadyRun){
        		alreadyRun = true;
        		recordedTime = (System.currentTimeMillis() - startTime) / 1000;
        		TelephonyManager manager = (TelephonyManager) getActivity().getSystemService(getActivity().TELEPHONY_SERVICE);
    			String carrierName = manager.getNetworkOperatorName();

    			RowItem items = new RowItem(carrierName, urlText, recordedTime);
    			rowItems.clear();
    			rowItems.add(items);
    			
    			//Save to database
    			ContentValues rw = new ContentValues();
    			rw.put("network", carrierName);
    			rw.put("url", urlText);
    			rw.put("delayinsec", Long.toString(recordedTime));
    			rw.put("dates", getTime());
    			db.add("delays", rw);
    			
    			adapter.notifyDataSetChanged();

    			// To dismiss the dialog
    			progress.dismiss();	
        		/*Toast.makeText(getActivity(), "Progress == 100", Toast.LENGTH_LONG).show();*/
        	}
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view,
                String url) {
            return null;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
        	super.onPageFinished(view, url);
        }
        
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        	Toast.makeText(getActivity(), "Error "+description, Toast.LENGTH_LONG).show();
        	progress.dismiss();	
        }

    };

	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Toast.makeText(getActivity(), menutitles[position], Toast.LENGTH_LONG).show();
	}
	
	public String getTime(){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		String test = sdf.format(cal.getTime());
		Log.e("TEST TIME", test);
		return test;
	} 
}

