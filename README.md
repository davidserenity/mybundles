### MyBundles Android Mobile Application ###

* This app monitors internet data usage on your phone and displays charts for usage patterns


### Installation ###

* The installation file *MyBundles.apk* is in bin folder; copy the file to your device and run it to install the application on your phone.

### Feedback ###

* Kindly send any feedback and suggestions to debukali@gmail.com